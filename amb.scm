;; This software is Copyright (c) Jeronimo C. Pellegrini, 2010-2019.
;;
;;  This program is free software; you can redistribute it and/or
;;  modify it under the terms of the GNU Affero General Public License as
;;  published by the Free Software Foundation; either version 3 of the
;;  License, or (at your option) any later version.
;; 
;;  This program is distributed in the hope that it will be useful, but
;;  WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;  Affero General Public License for more details.
;; 
;;  You should have received a copy of the GNU Affero General Public License
;;  along with this program; if not, write to:
;;    The Free Software Foundation, Inc.,
;;    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

;; amb.scm -- implements the AMB operator
;; 
;; SYNOPSIS:

;; * amb:  it is syntax (does NOT evaluate its arguments), and there are
;;         procedures to save and restore the state. SLOWER.
;; * amb+: it is a procedure (it WILL evaluate its arguments), with no way
;;         to save and restore the state. FASTER.

;; Suggested benchmarks (using iota from SRFI 1), assuming that there is
;; a "time" special form:
;;
;; (time (let ((x (amb/list (iota 100000)))) (require (> x 90000)) x))
;; (time (let ((x (amb+/list (iota 100000)))) (require+ (> x 90000)) x))

;;;
;;; AMB
;;;

;; (amb-reset)
;; :: resets the amb tree: this will make amb forget everything.
;;
;; (amb x1 x2 ...)
;; :: add "x1 x2 ..." to the set of possible expressions to evaluate
;;
;; (amb)
;; :: selects one of the expressions and continue
;;
;; (amb/list lst)
;; :: same as (amb x1 x2 ...), where lst is (list x1 x2 ...)
;;
;; (amb/random/list lst)
;; :: same as amb/list, but shuffles the list (this would make it look
;; :: like they are selected in random order)
;;
;; (require p)
;; :: if p is false, either bring the next answer or fail
;; :: (if p is false, call (amb) )
;; :: otherwise, do nothing.
;;
;; (amb-backup)
;; :: Returns a backup of the amb context. Does NOT change the
;; :: subsequent behavior of amb.
;;
;; (amb-save)
;; :: Returns a backup of the amb context. This WILL change the
;; :: subsequent behavior of amb, because the amb tree is DELETED
;; :: and no more values will be available.
;;
;; (amb-restore b)
;; :: Restores amb context from b, which is the value returned by
;; :: either amb-backup or amb-save.


;;;
;;; AMB+
;;;

;; (amb+-reset)
;; :: resets the amb tree: this will make amb+ forget everything.
;;
;; (amb+ x1 x2 ...)
;; :: add "x1 x2 ..." to the set of possible values (these will be
;; ;; evaluated).
;;
;; (amb+)
;; :: selects one of the possible values and continue. if there are
;; :: no more solutions to return, it will signal an error
;;
;; (amb+/list lst)
;; :: same as (amb+ x1 x2 ...), where lst is (list x1 x2 ...)
;;
;; (amb+/random x1 x2 ...)
;; :: same as amb+, but shuffles its arguments (this would make it look
;; :: like they are selected in random order)
;;
;; (amb+/random/list lst)
;; :: same as amb+/list, but shuffles the list (this would make it look
;; :: like they are selected in random order)
;;
;; (require+ p)
;; :: if p is false, either bring the next answer or fail
;; :: (if p is false, call (amb+) )
;; :: otherwise, do nothing.
;;
;;
;;;
;;; CHANGING THE FAILING BEHAVIOR
;;;
;;
;; When (amb) or (amb+) are called without arguments, and there is
;; nothing in the AMB tree, the default is to signal an error.
;; However, this can be modified by setting these two procedures:
;;
;; (amb-fail-action)  is what is done when there are no more possibilities
;; for amb
;;
;; (amb+-fail-action) is the same, for amb+.
;;
;; AND calling amb-reset or amb+-reset afterwards.
;;
;; Example:
;;
;; (define (amb-fail-action) -inf.0)  ; when failing, return -inf.0
;; (amb-reset)
;; (let ((x (amb 1.0 2.0 3.0))) (require (> x 5.0)) x)  =>  -inf.0
;;
;; (define (amb+-fail-action) 42)  ; when failing, return 42
;; (amb+-reset)
;; (let ((x (amb 1.0 2.0 3.0))) (require (> x 5.0)) x)  =>  42


(import (srfi 27)) ; for shuffling arguments

(begin

(define (swap! vec i j)
  (let ((tmp (vector-ref vec i)))
    (vector-set! vec i (vector-ref vec j))
    (vector-set! vec j tmp)))
  
(define (shuffle lst)
  (let* ((vec (list->vector lst))
         (len (vector-length vec)))
    (let loop ((i 0))
      (if (< i len)
          (let ((r (+ i (random-integer (- len i)))))
                (swap! vec i r)
                (loop (+ i 1)))
          (vector->list vec)))))

;;;
;;; AMB
;;;
  
;; called when (amb) is called and there is no more answers available.
(define (fail) (error "amb: no more choices!"))

(define amb-fail-action fail)

;; resets the amb tree: this will make amb forget everything.
(define (amb-reset)
  (set! fail amb-fail-action))

;; Classical implementation of AMB as a macro.
(define-syntax amb 
  (syntax-rules ()
    ((amb) (fail))
    ((amb expression) expression) 
    ((amb expression ...) 
     (let ((fail-save fail)) 
       ((call/cc 
         (lambda (k-success)
           (call/cc
            (lambda (k-failure)
              (set! fail
                (lambda () (k-failure #f)))
              (k-success
               (lambda ()
                 expression))))
           ... 
           (set! fail fail-save)
           fail-save)))))))


;; This implementation of AMB allows for an easy way to backup
;; the AMB context.

(define (amb-backup)
  ;; Does *not* change the current state of the AMB tree
  (let ((amb-fail-action-bak amb-fail-action)
        (amb-fail-bak fail))
    (list amb-fail-action-bak amb-fail-bak)))

(define (amb-save)
  ;; Completely deletes the AMB tree after retrieving and
  ;; returning it.
  (let ((amb-fail-action-bak amb-fail-action)
        (amb-fail-bak fail))
    (amb-reset)
    (list amb-fail-action-bak amb-fail-bak)))

(define (amb-restore lst)
  (set! amb-fail-action (car lst))
  (set! fail (cadr lst)))

;; if p is false, either bring the next answer or fail
;; (if p is false, call (amb) )
;; otherwise, do nothing.
(define (require p)
  (if (not p) (amb)))

(define (amb/list lst)
  (let loop ((lst lst))
    (if (null? lst)
        (amb)
        (amb (car lst) (loop (cdr lst))))))

(define (amb/random/list lst) (amb/list (shuffle lst)))

;;;
;;; AMB+
;;;

(define (now)
  (call-with-current-continuation 
   (lambda (k) (k k))))

;; The AMB+ set.
(define amb+-set '())

(define (amb+-reset)
  (set! amb+-set '()))

;; %amb+-add is
;; NOT to be called by the user. It is used internally by amb+.
(define (%amb+-add k)
  (set! amb+-set (cons k amb+-set)))

;; %amb+-delete-one is
;; NOT to be called by the user. It is used internally by amb+-fail.
(define (%amb+-delete-one)
  (set! amb+-set (cdr amb+-set)))

(define (amb+-fail-action) (error "amb+: no more choices!"))

(define (amb+-fail)
  (if (null? amb+-set)
      (amb+-fail-action)
      (let ((back (car amb+-set)))
        (%amb+-delete-one)
        (back back))))

(define (amb+/list args)
  (let ((choices args))
    (let ((cc (now)))
      (cond ((null? choices) (amb+-fail))
            ((pair? choices)
             (let ((choice (car choices)))
               (set! choices (cdr choices))
               (%amb+-add cc)
               choice))
            (else (error "amb choices must be a list"))))))

(define (amb+ . args) (amb+/list args))

(define (amb+/random . args) (amb+/list (shuffle args)))

(define (amb+/random/list args) (amb+/list (shuffle args)))

(define (require+ p)
  (if (not p) (amb+)))
       
) ;; begin
