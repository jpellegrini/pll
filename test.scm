(load "pll-standalone.scm")
(import (srfi 64))
(test-begin "PLL")

;;;
;;; Prolog examples.
;;; Please see the manual for help with the syntax.
;;;

(define write-line
  (lambda args
    (for-each (lambda (x)
                (display x)
                (display " "))
              args)
    (newline)))


;;;
;;; AMB
;;;

;; Not too many tests here.

(test-eq "amb"
  0
  (let ((x (amb 10 20 30 40))
        (y (amb -40 -30 -20 -10)))
    (require (zero? (+ x y)))
    (+ x y)))

(test-eq "amb+"
  0
  (let ((x (amb+ 10 20 30 40))
        (y (amb+ -40 -30 -20 -10)))
    (require+ (zero? (+ x y)))
    (+ x y)))

(test-eq "amb/list"
  #t
  (let ((list-x '(10 20 30 40))
        (list-y '(-40 -30 -20 -10)))
    (let ((x (amb/list list-x))
          (y (amb/list list-y)))
      (and (memq x list-x)
           (memq y list-y)
           #t))))

(test-eq "amb+/list"
  #t
  (let ((list-x '(10 20 30 40))
        (list-y '(-40 -30 -20 -10)))
    (let ((x (amb+/list list-x))
          (y (amb+/list list-y)))
      (and (memq x list-x)
           (memq y list-y)
           #t))))


(test-eq "amb/list"
  #t
  (let ((list-x '(10 20 30 40))
        (list-y '(-40 -30 -20 -10)))
    (let ((x (amb/random/list list-x))
          (y (amb/random/list list-y)))
      (and (memq x list-x)
           (memq y list-y)
           #t))))

(test-eq "amb+/list"
  #t
  (let ((list-x '(10 20 30 40))
        (list-y '(-40 -30 -20 -10)))
    (let ((x (amb+/random/list list-x))
          (y (amb+/random/list list-y)))
      (and (memq x list-x)
           (memq y list-y)
           #t))))

(test-eq "amb/list"
  0
  (let ((x (amb/list '(10 20 30 40)))
        (y (amb/list '(-40 -30 -20 -10))))
    (require (zero? (+ x y)))
    (+ x y)))

(test-eq "amb+/list"
  0
  (let ((x (amb+/list '(10 20 30 40)))
        (y (amb+/list '(-40 -30 -20 -10))))
    (require+ (zero? (+ x y)))
    (+ x y)))


;;;
;;; UNIFICATION
;;;

(test-eq ""
  #f
  (unify 1 2))

(test-equal ""
  '(?a . 2)
  (assoc '?a (unify '?a 2)))

(test-equal ""
  '(?a . 2)
  (assoc '?a (unify '(?a) '(2))))

(test-equal ""
  '((?a . 2) (?b . 1))
  (let ((x (unify '(?a 1) '(2 ?b))))
    (list 
     (assq '?a  x)
     (assq '?b  x))))

(test-equal ""
  '((?a . 2) (?b . 1))
  (let ((x (unify '(?a (1 3)) '(2 (?b 3)))))
    (list 
     (assq '?a  x)
     (assq '?b  x))))

(test-eq ""
  #f
  (unify '(?a) 2))

(test-eq ""
  #f
  (unify '(?a ?b) 2))

(test-eq ""
  #f
  (unify '(?a (1 3)) '(2 (?b 4))))

(test-eq ""
  #f
  (unify '(?a (1 3)) '(2 (?b 3 4))))

;;;
;;; PROLOG
;;;

;; Prolog program: 
;; a(X) :- b(X), !, c(X).
;; b(1).
;; b(2).
;; c(2).
;;
;; We ask a(X), it succeeds for X=1, but cannot
;; backtrack so it fails!
(test-eq ""
  #f
  (prolog+cut '(( (a ?x) (b ?x) ! (c ?x) )
                ( (b 1) )
                ( (b 2) )
                ( (c 2) ))
              '((a ?x))))

;; Same as in previous example, but we do not use
;; the cut, so it succeeds.
;; Prolog program: 
;; a(X) :- b(X), !, c(X).
;; b(1).
;; b(2).
;; c(2).
;;
;; We ask a(X). Internally, it succeeds for X=1, but since this
;; doesn't work, Prolog backtracks and tries X=2, then succeeds.
(test-equal ""
  '((?z . 2))
  (pure-prolog '(( (a ?x) (b ?x) (c ?x) )
                 ( (b 1) )
                 ( (b 2) )
                 ( (c 2) ))
               '((a ?z))))

;; Prolog program:
;; f(0).
;; f(Z) :- g(Z).
;; p(A) :- f(A).
;; g(10).
;;
;;; f(X).
;;; the answer is the substitution X->0.
(test-equal ""
  '((?x . 0))
  (pure-prolog '(( (f 0)  )
                 ( (f ?z) (g ?z) )
                 ( (p ?a) (f ?a) )
                 ( (g 10) ))
               '((f ?x))))


;; Prolog program:
;; f(0).
;; f(Z) :- g(Z).
;; p(Z,Y) :- f(Z),g(Y).
;; g(10).
;;
;;; p(10,10) --> succeeds
;;; p(0,10)  --> also succeeds
;;; p(3,10)  --> fails
(test-equal ""
  '()
  (pure-prolog '(( (f 0) )
                 ( (f ?z) (g ?z))
                 ( (p ?z ?y) (f ?z) (g ?y))
                 ( (g 10) ))
               '((p 10 10))))

(test-equal ""
  '()
  (pure-prolog '(( (f 0) )
                 (  (f ?z) (g ?z))
                 (  (p ?z ?y) (f ?z) (g ?y))
                 (  (g 10) ))
               '((p 0 10))))

(test-eq ""
  #f
  (pure-prolog '(( (f 0)  )
                 ( (f ?z) (g ?z))
                 ( (p ?z ?y) (f ?z) (g ?y))
                 ( (g 10) ))
               '((p 3 10))))

(define (assoc-check elt expexted db)
  (let ((res (assoc elt db)))
    (equal? expexted (cdr res))))

;; f(0).
;; f(Z) :- g(Z).
;; h(3).
;; h(4).
;; p(Z,Y,S) :- f(Z),g(Y),h(S)
;; g(10).
;;
;;; p(10,D,A)
;;; succeeds with {D->10, A->3}
;;;
;;; if we put the additional goal q(A), then
;;; p(10,D,A), q(A)
;;; succeeds with {D->10, A->4}
(test-assert ""
  (let ((res (pure-prolog '(( (f 0) )
                            ( (f ?z) (g ?z))
                            ( (h 3) )
                            ( (h 4) )
                            ( (p ?z ?y ?s) (f ?z) (g ?y) (h ?s))
                            ( (g 10) ))
                          '((p 10 ?d ?a)))))
    (and (assoc-check '?a 3 res)
         (assoc-check '?d 10 res))))

(test-assert ""
  (let ((res (pure-prolog '(( (f 0) )
                            ( (f ?z) (g ?z))
                            ( (h 3) )
                            ( (h 4) )
                            ( (q 4) )
                            ( (p ?z ?y ?s) (f ?z) (g ?y) (h ?s))
                            ( (g 10) ))
                          '((p 10 ?d ?a) (q ?a)))))
    (and (assoc-check '?a 4 res)
         (assoc-check '?d 10 res))))

(test-assert ""
  (let ((res (pure-prolog '(( (f 0) )
                            ( (f ?z) (g ?z))
                            ( (h 3) )
                            ( (h 4) )
                            ( (p ?z ?y ?s) (f ?z) (g ?y) (h ?s))
                            ( (g 10)  ))
                          '((p 10 ?d ?a) (h ?a)))))
        (and (assoc-check '?a 3 res)
         (assoc-check '?d 10 res))))



;; Changing a variable name in the goal gives the same result as
;; above:

(test-assert ""
  (let ((res (pure-prolog '(( (f 0) )
                            ( (f ?z) (g ?z))
                            ( (h 3) )
                            ( (h 4) )
                            ( (p ?z ?y ?s) (f ?z) (g ?y) (h ?s))
                            ( (g 10) ))
                          '( (p 10 ?y ?s) (h ?s) ) )))
    (and (assoc-check '?s 3 res)
         (assoc-check '?y 10 res))))



;; Fails
(test-eq ""
    #f
    (prolog+cut '(( (a ?x) (b ?x) ! (c ?x) )
                  ( (b 1) )
                  ( (b 2) )
                  ( (c 2) ))
                '((a ?x))))

;; Succeeds with Z=2
(test-equal ""
  '((?z . 2))
  (pure-prolog '(( (a ?x) (b ?x) (c ?x) )
                 ( (b 1) )
                 ( (b 2) )
                 ( (c 2) ))
               '((a ?z))))

;; Succeeds with Z=2
(test-equal ""
  '((?z . 2))
  (pure-prolog '(( (a ?x) (b ?x) (c ?x) )
                 ( (b 1) )
                 ( (b 2) )
                 ( (c 2) ))
               '((a ?z))))


;; First prints "Grandson is: johnny", then returns the
;; substitution Who=johnny.
(test-equal ""
  '( (((?who . johnny)) ())
     "Grandson is: johnny")
  (let* ((res #f)
         (str (with-output-to-string
                (lambda ()
                  (set! res (prolog+built-ins '( ((father john johnny))
                                                 ((father old-john john))
                                                 ((grandpa ?a ?b) (father ?a ?x) (father ?x ?b)) )
                                              '( (grandpa old-john ?who)  (write "Grandson is: " ?who) )))))))
    (list res str)))
              

;; First prints "Grandson is: johnny", then returns the
;; substitution Who=johnny.
(test-equal ""
  '(((?who quote johnny))
    "Grandson is:  johnny \n")
  (let* ((res #f)
         (str (with-output-to-string
                (lambda ()
                  (set! res (prolog+scheme '( ((father 'john 'johnny))
                                              ((father 'old-john 'john))
                                              ((grandpa ?a ?b) (father ?a ?x) (father ?x ?b)) )
                                           '( (grandpa 'old-john ?who)  ( (write-line "Grandson is: " ?who) ))))))))
    (list res str)))
  
;; Prints "5 5" then succeeds with X=Y=5
(test-equal ""
  "5   5 \n"
  (parameterize
      ((current-output-port (open-output-string)))
    (let ((res (prolog+scheme '( ((a 5))
                                 ((b 3))
                                 ((b 5)))
                              '( (a ?x) (b ?y) ((= ?x ?y)) ((write-line ?x " " ?y))))))
      (and (assoc-check '?y 5 res)
           (assoc-check '?x 5 res)))
    (get-output-string (current-output-port))))
             
;; Succeeds
(test-equal ""
  '()
  (prolog+scheme '( )
                 '( ((= 5 5)))))

;; Fails
(test-eq ""
  #f
  (prolog+scheme '( )
                 '( ((= 2 5)))))


;; The variable ?y is not defined, so this signals an error:
(test-error
 (prolog+local '( ((f ?x ?y)
                   ((= ?y (* 2 ?x)))
                   ((write-line 'OK: ?y))))
               '( (f 3 ?a) )))

;; Succeeds with A=6
(test-equal ""
  '((?a . 6))
  (parameterize ((current-output-port (open-output-string)))
    (prolog+local '( ((f ?x ?y)
                      (is ?y (* 2 ?x))
                      ((write-line 'OK: ?y))))
                  '( (f 3 ?a) ))))


;; This solves the Towers of Hanoi problem, in Prolog.
(define prolog-hanoi
  '(((hanoi ?n) (hanoi-aux ?n 3 1 2))
    ((hanoi-aux 0 ?a ?b ?c) ! )
    ((hanoi-aux ?n ?a ?b ?c)
     (is ?nn (- ?n 1))
     (hanoi-aux ?nn ?a ?c ?b)
     (mova ?a ?b)
     (hanoi-aux ?nn ?c ?b ?a))
    ((mova ?de ?para)
     ((display ?de))
     ((display " --> "))
     ((display ?para))
     ((newline)))))

;; We call it for 3 discs:
(test-equal "hanoi"
  '(()
    "3 --> 1\n3 --> 2\n1 --> 2\n3 --> 1\n2 --> 3\n2 --> 1\n3 --> 1\n")
  (let* ((res #f)
         (str (with-output-to-string
                (lambda ()
                  (set! res (prolog+cut prolog-hanoi '((hanoi 3))))))))
    (list res str)))

;; It wil print the solution, and then succeed:
;; 3 --> 1
;; 3 --> 2
;; 1 --> 2
;; 3 --> 1
;; 2 --> 3
;; 2 --> 1
;; 3 --> 1
;; ()


;; Prolog program:
;; f(X) :- asserta(h(1)).
;; g(X) :- h(X).
;;
;; f(W), g(Z) --> succeeds with Z=1
(test-equal ""
  '((?z . 1))
  (prolog+meta '(( (f ?x) (asserta (h 1)))
                 ( (g ?x) (h ?x)))
               '((f ?w) (g ?z))))

(define prolog-assert-ok '( ((f ?x) (g ?x)
                             (asserta (h 3))
                             (h ?x))
                            ((g 2))
                            ((g 3))))

;; Succeeds with A=3
(test-equal ""
  '((?a . 3))
  (prolog+meta prolog-assert-ok '((f ?a))))

(define prolog-assert-fail '( ((f ?x) (h ?x)
                                    (asserta (h 3))
                                    (g ?x))
                            ((g 2))
                            ((g 3))))

;; Fails:
(test-eq ""
  #f
  (prolog+meta prolog-assert-fail '((f ?a))))


;; Prolog program:
;; f(2).
;; f(3).
;; g(1) :- retract(f(2)).
;; g(1) :- f(3).
(define prolog-retract '( ((f 2))
                          ((f 3))
                          ((g 1) (retract ((f 2))))
                          ((g 1) (f 3))))

;; f(X) succeeds with X=2.
(test-equal ""
  '((?x . 2))
  (prolog+meta prolog-retract '((f ?x))))

;; g(X) will cause retract(f(2)) to be evaluated, so
;; g(1), f(X) succeeds with X=3.
(test-equal ""
  '((?x . 3))
  (prolog+meta prolog-retract '((g 1) (f ?x))))

;; The Prolog with cut has all features from the previous ones:
(test-equal ""
  '((?x . 3))
  (prolog+cut prolog-retract '((g 1) ! (f ?x))))

;; f(2), g(1) succeeds, because f(2) is evaluated before
;; g(1).
(test-equal ""
    '()
  (prolog+meta prolog-retract '((f 2) (g 1))))

;; If we evaluate g(1) first, it fails:
(test-eq ""
  #f
  (prolog+meta prolog-retract '((g 1) (f 2))))

;; Error: ?b is not bound:
(test-error "unbound var ?b"
 (prolog+cut '()
             '((is ?a 5)
               (is ?x (+ ?a ?b)))))
 
;; We can use Scheme lists in Prolog. This is append/2:
(define prolog-append '( ((append (?x . ?y) ?z (?x . ?w))
                          (append ?y ?z ?w))
                         ((append () ?x ?x))))

;; Succeeds with A=(0 1)
(test-equal "append 1"
  '((?a 0 1))
  (prolog+cut prolog-append
              '( (append (0) (1) ?a) )))

;; Succeeds with A=(1)
;; (this one is very nice, and shows some cool Prolog programming techniques)
(test-equal "append 2"
  '((?a 1))
  (prolog+cut prolog-append
              '( (append (0) ?a (0 1)) )))

;; Succeeds with A=(1) and D=0.
(test-assert "append 3"
  (let ((res (prolog+cut prolog-append
                         '( (append (0) ?a (?d 1) )))))
    (and (assoc-check '?d 0 res)
         (assoc-check '?a '(1) res))))

;; Difference lists:
(define prolog-difflist '( ((count (- ?x ?x1) 0)
                            ((unify '?x '?x1))
                            !)
                           ((count (- (?h . ?t) ?t1) ?n)
                            (count (- ?t ?t1) ?m)
                            (is ?n (+ ?m 1)))))

;; Succeeds with N=2
(test-equal "difflist"
  '((?n . 2))
  (prolog+cut prolog-difflist
              '( (count (- (1 2 . ?a) ?a) ?n) )))

;; Succeeds, but with empty sub (calls to unify via Scheme
;; FFI do NOT add bindings)
(test-equal ""
  '()
  (prolog+cut '() '( ((unify '?a '?b)))))

(test-equal ""
  '()
  (prolog+cut '() '( ((unify '?a 'b)))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '() '( ((unify 'a 'b)))))

;; This will loop forever
(test-error "amb tree exhauster"
            (parameterize ((current-output-port (open-output-string)))
              (prolog+cut '(((rep))
                          ((rep) (rep)))
                          '( (rep) ((write-line 'z)) ((fail)) ))))

;;not(P) :- call(P), !, fail.
;;not(P).
(define pfail (lambda ()  #f))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '(((f) (a) ! (b))
                ((a)))
              '((f))))

;; Succeeds with NO substitution
(test-equal ""
  '()
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((not (not (f ?z))))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((not (not (not (f ?z)))))))

;; Succeeds:
(test-equal ""
  '()
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((not (not (not (not (f ?z))))))))

;; fails:
(test-eq ""
  #f
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((not (not (not (not (not (f ?z)))))))))

;; Succeeds with ?z -> a
(test-equal ""
  '((?z . a))
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((f ?z))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((not (f ?z)))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((not (f a)))))

;; Succeeds:
(test-equal ""
  '()
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x))
                ((f a)) )
              '((not (f b)))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '() '( ((pfail)) )))

;; Succeeds:
(test-equal ""
  '()
  (prolog+cut '(((not ?x) ?x ! ((pfail)))
                ((not ?x)))
              '((not ((pfail))))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '(((go ?x) ?x)
                ((f a) ((write-line 'HELLO))))
              '( (go ((pfail))) )))

;; Succeeds:
(test-equal ""
  '()
  (prolog+cut '(((f a) (g b))
                ((g b) (h c))
                ((h c)))
              '( (f a) )))

;; Succeeds:
(test-equal ""
  '()
  (prolog+cut '(((f a)))
              '((f a))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '(((f a)))
              '((f b))))

;; Fails:
(test-eq ""
  #f
  (prolog+cut '(( (a ?x) (b ?x) ! (c ?x) )
                ( (b 1) )
                ( (b 2) )
                ( (c 2) ))
              '((a ?x))))

;; f(0).
;; f(Z) :- g(Z).
;; p(Z,Y) :- f(Z),g(Y).
;; g(10)
;;
;;; p(10,10)  --> also succeeds with p(0,10)
(test-equal ""
  '()
  (prolog+cut '(((f 0))
                ((f ?z) (g ?z))
                ((p ?z ?y) (f ?z) (g ?y))
                ((g 10)))
              '((p 10 10))))

(test-equal ""
  '()
  (prolog+cut '(((f 0))
                ((f ?z) (g ?z))
                ((p ?z ?y) (f ?z) (g ?y))
                ((g 10)))
              '((p 0 10))))


;;; p(3,10)    --> fails
(test-eq ""
  #f
  (prolog+cut '(((f 0))
                ((f ?z) (g ?z))
                ((p ?z ?y) (f ?z) (g ?y))
                ((g 10)))
              '((p 3 10))))

;; f(0).
;; f(Z) :- g(Z).
;; h(3).
;; h(4).
;; p(Z,Y,S) :- f(Z),g(Y),h(S)
;; g(10).
;;
;;; p(10,D,A)
;; Succeeds with {D->10, A->3}, {p(10,Y,S)}
(test-assert ""
  (let ((res (prolog+cut '(((f 0))
                           ((f ?z) (g ?z))
                           ((h 3))
                           ((h 4))
                           ((p ?z ?y ?s) (f ?z) (g ?y) (h ?s))
                           ((g 10)))
                         '((p 10 ?d ?a)))))
    (and (assoc-check '?a 3 res)
         (assoc-check '?d 10 res))))

;; D->10, A->4
(test-assert ""
  (let ((res (prolog+cut '(((f 0))
                           ((f ?z) (g ?z))
                           ((h 3))
                           ((h 4))
                           ((q 4))
                           ((p ?z ?y ?s) (f ?z) (g ?y) (h ?s))
                           ((g 10)))
                         '((p 10 ?d ?a) (q ?a)))))
    (and (assoc-check '?a 4 res)
         (assoc-check '?d 10 res))))


;; Succeeds, A->1
(test-equal ""
  '((?a . 1))
  (prolog+cut '(((f 0))
                ((f 1))
                ((g 1))
                ((h ?x) (f ?x) (g ?x)))
              '((f ?a) (h ?a))))

;; Prolog program:
;; f(0).
;; f(1).
;; g(1).
;; h(X) :- f(X), g(X).
;;
;; h(A), !, f(A) succeeds with A=1:
;; - h(X) depends on f(X) and g(X), so
;; prolog tries X=0, but fails for g(X),
;; backtracks, and finds X=1. THEN it
;; goes past the cut, and finds f(A),
;; which is true.
(test-equal ""
  '((?a . 1))
  (prolog+cut '(((f 0))
                ((f 1))
                ((g 1))
                ((h ?x) (f ?x) (g ?x)))
              '((h ?a) ! (f ?a))))

;; If we change the order in the goal, Prolog fails:
(test-eq "change order in goal with cut"
  #f
  (prolog+cut '(((f 0))
                ((f 1))
                ((g 1))
                ((h ?x) (f ?x) (g ?x)))
              '((f ?a) ! (h ?a))))

;; f(0).
;; f(Z) :- g(Z).
;; h(3).
;; h(4).
;; p(Z,Y,S) :- f(Z),g(Y),h(S)
;;
;;; p(10,D,A)
;; retorna {D->10, A->3}, {}}
(test-assert ""
  (let ((res (prolog+cut '(((f 0))
                           ((f ?z) (g ?z))
                           ((h 3))
                           ((h 4))
                           ((p ?z ?y ?s) (f ?z) (g ?y) (h ?s))
                           ((g 10)))
                         '((p 10 ?d ?a) (h ?a)))))
    (and (assoc-check '?a 3 res)
         (assoc-check '?d 10 res))))

;; We define a graph by declaring its edges, then
;; define a reach/2 predicate.
;;
;; edge(a,b).
;; edge(a,c).
;; edge(c,b).
;; edge(c,d).
;; edge(d,e).
;;
;; reach(A,B) :- edge(A,B).
;; reach(A,B) :- edge(A,X), reach(X,B).
(define graph '( ((edge a b))
                 ((edge a c))
                 ((edge c b))
                 ((edge c d))
                 ((edge d e))
                 ;;
                 ((reach ?a ?b) (edge ?a ?b))
                 ((reach ?a ?b) (edge ?a ?x)
                                (reach ?x ?b))))

(test-eq "graph: can't reach"
  #f
  (pure-prolog graph '( (reach b e) )))

;; Succeeds, X=1
(test-equal ""
  '((?x . 1))
  (pure-prolog '(((f 1))
                 ((f 2))
                 ((f 3)))
               '((f ?x))))

;; (amb+) would brings other solutions (X=2, X=3)

;; Prints two substitutions (1, 2) then fails.
(define l '())
(test-equal ""
  '(#f "1 \n2 \n")
  (let* ((res #f)
         (str (with-output-to-string
                (lambda ()  
                  (set! res (prolog+scheme '(((f 1))
                                             ((f 2)))
                                           '((f ?x) ((set! l (write-line ?x))) ((pfail)))))))))
    (list res str)))

(test-end "PLL")

